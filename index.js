let trainer = {
	name: 'Ash Ketchum',
	age: 10,
	pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
	friends: {
		hoenn: ['May', 'Max'],
		kanto: ['Brock', 'Misty']
	},
	talk: function(){
		console.log(trainer.pokemon[0] + '! ' + 'I choose you!');
	}
}
console.log(trainer);
console.log('Result of dot notation:');
console.log(trainer.name);
console.log('Result of square bracket notation:');
console.log(trainer['pokemon'])
console.log('Result of talk method:');
trainer.talk();

function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;
}

let a = new Pokemon('Pikachu', 12);
let b = new Pokemon('Geodude', 8);
let c = new Pokemon('Mewtwo', 100);

console.log(a);
console.log(b);
console.log(c);

b.tackle = function(){
	a.health = a.health - b.attack;
	console.log(b.name + ' tackled ' + a.name)
	console.log(a.name + "'s health is reduced to " + a.health)
	if(a.health < 0){
		console.log(a.name + ' fainted')
	}
	else{
		
	}
}

b.tackle();

console.log(a);

c.tackle = function(){
	b.health = b.health - c.attack;
	console.log(c.name + ' tackled ' + b.name)
	console.log(b.name + "'s health is reduced to " + b.health)
	if(b.health < 0){
		console.log(b.name + ' fainted');
	}
	else{
		
	}
}

c.tackle();

console.log(b);